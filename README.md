# My Doom Private Config
This is my personal configuration for [Doom Emacs](https://github.com/doomemacs/doomemacs).
As of now, it only configures the search path for Projectile Projects, and loads the Z80 syntax highlighting file required for highlighting z80 assembly source (as .z80 files, or use `M-x z80-mode` on a opened assembly file (.asm))
